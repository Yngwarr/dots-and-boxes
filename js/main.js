/* used for svg manipulations */
const xmlns = "http://www.w3.org/2000/svg";
/* size of the game field, boxes; alterable */
const GRID_SHAPE = [8, 8];
/* top-left corner of the game field, px; also alterable */
const ORI = [32, 32];
/* radius of a dot when not hovered, px; alterable, for hover r check css */
const DOT_R = 8;
/* distance between neighbor dots, px */
const WALL_LEN = 64;

/* root svg element */
let svg;
/* group elements */
let g_boxes;
let g_walls;
let g_dots;
/* controllable line, follows the pointer when presented */
let line = null;
/* of dots to be connected */
let conn = [];

/* the game controller, WIP */
let game;

function init() {
	/* setting up the shortcuts */
	svg = document.querySelector('svg');
	g_boxes = document.getElementById('boxes');
	g_walls = document.getElementById('walls');
	g_dots = document.getElementById('dots');
	game = new Game(GRID_SHAPE);
	populate();
	svg.addEventListener('mousemove', mousemove);
}

/* creates them dots */
function populate() {
	let [w, h] = GRID_SHAPE;
	let [ox, oy] = ORI;
	/* numerating dots is efficient for game logging */
	let n = 0;
	let n_box = 0;
	for (let x = 0; x < w; ++x) {
		for (let y = 0; y < h; ++y) {
			let dot = document.createElementNS(xmlns, 'circle');
			dot.classList.add('dot');
			dot.setAttributeNS(null, 'cx', ox + x*WALL_LEN);
			dot.setAttributeNS(null, 'cy', oy + y*WALL_LEN);
			dot.setAttributeNS(null, 'r', DOT_R);
			dot.dataset.x = x;
			dot.dataset.y = y;
			dot.dataset.n = n++;
			dot.addEventListener('click', dot_click);
			g_dots.appendChild(dot);
			if (x === w - 1 || y === h - 1) continue;
			let box = document.createElementNS(xmlns, 'rect');
			box.classList.add('box');
			box.setAttributeNS(null, 'x', ox + x*WALL_LEN);
			box.setAttributeNS(null, 'y', oy + y*WALL_LEN);
			box.setAttributeNS(null, 'width', WALL_LEN);
			box.setAttributeNS(null, 'height', WALL_LEN);
			// TODO correct ns
			box.dataset.n = n_box++;
			g_boxes.appendChild(box);
		}
	}
}

/* checks if pt = [x, y] is placed in a circle with center ori = [cx, cy],
 * radius = r */
function in_circle(pt, ori, r) {
	return (pt[0] - ori[0])**2 + (pt[1] - ori[1])**2 <= r**2;
}

/* returns the closest to p2 = [x2, y2] point in a circle
 * with center p1 = [x1, y1], radius r_dest */
function encircle(p1, p2, r_dest) {
	let dx = p2[0] - p1[0];
	let dy = p2[1] - p1[1];
	let qsum = dx**2 + dy**2;
	if (qsum <= r_dest**2) return p2;
	let r = qsum**.5;
	return [
		p1[0] + (dx / r) * r_dest,
		p1[1] + (dy / r) * r_dest
	];
}

function set_pl(elem, pl) {
	elem.classList.remove(`pl${pl ? 0 : 1}`);
	elem.classList.add(`pl${pl}`);
}

function conn_to_edge(c) {
	return c.map((e) => { return parseInt(e.dataset.n); });
}

/* ====== [CALLBACKS] ====== */

function dot_click(e) {
	let dot = e.target;
	let x = parseInt(dot.getAttribute('cx'));
	let y = parseInt(dot.getAttribute('cy'));
	/* the first click, line is not presented */
	if (!line) {
		/* create a line */
		line = document.createElementNS(xmlns, 'line')
		line.classList.add('wall');
		set_pl(line, game.turn);
		line.setAttributeNS(null, 'x1', x);
		line.setAttributeNS(null, 'y1', y);
		line.setAttributeNS(null, 'x2', x);
		line.setAttributeNS(null, 'y2', y);
		g_walls.appendChild(line);
		conn.push(dot);
		// TODO paint the dot
		set_pl(dot, game.turn);
		return;
	}
	/* the second click, line is presented */
	let l = line;
	line = null;
	let ox = parseInt(l.getAttribute('x1'));
	let oy = parseInt(l.getAttribute('y1'));
	/* when the dot is reachable, make a turn */
	if (!(x === ox && y === oy) && in_circle([x, y], [ox, oy], WALL_LEN)) {
		conn.push(dot);
		let [succ, scored] = game.connect(conn_to_edge(conn));
		conn = [];
		if (succ) {
			l.setAttributeNS(null, 'x2', x);
			l.setAttributeNS(null, 'y2', y);
			set_pl(dot, game.turn);
			if (!scored) game.toggle_turn();
		} else {
			l.parentElement.removeChild(l);
		}
		return;
	}
	/* when the dot is unreachable, let the player start again */
	conn.pop();
	l.parentElement.removeChild(l);
}

function mousemove(e) {
	/* don't move the line's end when it's not presented */
	if (!line) return;
	let p1 = [
		parseFloat(line.getAttribute('x1')),
		parseFloat(line.getAttribute('y1'))
	];
	/* don't let the line leave its neighborhood */
	let p2 = encircle(p1, [e.clientX, e.clientY], WALL_LEN);
	line.setAttribute('x2', p2[0]);
	line.setAttribute('y2', p2[1]);
}

function vec_sort([a, b]) {
	return a > b ? [b, a] : [a, b];
}
