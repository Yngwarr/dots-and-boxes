/* the game controller; used to keep the record of who is winning, what belongs
 * to whom, when the game is over, etc. */
class Game {
	constructor(shape) {
		this.turn = 0;
		/* [x, y] where x and y are numbers of dots */
		this.edges = [];
		/* numbers of "dame" for boxes */
		this.boxes = [];
		for (let i = 0; i < (shape[0]-1)*(shape[1]-1); ++i) {
			this.boxes.push(4);
			//this.boxes.push(1);
		}
		this.log = [];
		this.pl = [
			/* no bullshit, just indices */
			{edges: [], dots: [], boxes: []},
			{edges: [], dots: [], boxes: []}
		];
		this.shape = shape;
	}
	toggle_turn() {
		this.turn = this.turn ? 0 : 1;
		return this.turn;
	}
	are_connected(a, b) {
		return !!_.find(this.edges, ([x, y]) => { return x === a && y === b; });
	}
	connect(edge) {
		let [a, b] = vec_sort(edge);
		if (this.are_connected(a, b)) return [false, false];
		/* 'hurt' the boxes */
		let scored = false;
		let x = ((a / GRID_SHAPE[0]) << 0);
		let y = ((a % GRID_SHAPE[1]) << 0);
		let bs = [a - x];
		if (a === b - 1) {
			/* vertical */
			bs.push(bs[0] - GRID_SHAPE[1] + 1);
		} else if (a === b - GRID_SHAPE[1]) {
			/* horizontal */
			// wrap zone; it's just a silly hack; it's 2 a.m., can't talk,
			// just push it already so we all (me) can go to sleep
			const bb = bs[0];
			if (y === GRID_SHAPE[1] - 1) {
				bs.pop();
			}
			if (y !== 0) bs.push(bb - 1);
		} else {
			console.warn("Who's cheating? 0_o");
			return [false, false];
		}
		bs = bs.filter((b) => {
			return b >= 0 && b < (GRID_SHAPE[0]-1)*(GRID_SHAPE[1]-1);
		});
		bs.forEach((b) => {
			this.boxes[b]--;
			if (this.boxes[b] === 0) {
				scored = true;
				this.pl[this.turn].boxes.push(b);
				set_pl(document.querySelector(`.box[data-n="${b}"]`), this.turn);
			}
		}, this);
		/* draw the edge */
		this.edges.push([a, b]);
		this.pl[this.turn].edges.push(this.edges.length - 1);
		return [true, scored];
	}
}
